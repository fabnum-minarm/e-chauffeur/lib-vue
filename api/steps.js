import AbstractCampusCRUDQuery from './abstract/campus-crud-query';

export const ENTITY_PLURAL = 'steps';
export const ENTITY = 'step';

export default class StepsQuery extends AbstractCampusCRUDQuery {
  static get ENTITY() {
    return ENTITY;
  }

  static get ENTITY_PLURAL() {
    return ENTITY_PLURAL;
  }

  get driverRoutePrefix() {
    return this.driver ? `/drivers/${encodeURIComponent(this.driver)}` : null;
  }

  getEndpoint(...params) {
    const base = [
      this.constructor.baseEndpoint,
      ...params.map((f) => encodeURIComponent(f)),
    ].join('/');
    return [
      this.campus ? this.campusRoutePrefix : '',
      this.driver ? this.driverRoutePrefix : '',
      base,
    ].join('');
  }

  setDriver(driver) {
    this.driver = driver;
    return this;
  }
}
