const { API_URL } = process.env;
const API_URL_WS = API_URL.replace(/^http/, 'ws');

const CONTENT_SECURITY_POLICY = (() => {
  const policies = {
    'connect-src': `'self' ${API_URL} ${API_URL_WS}`,
    'font-src': 'fonts.gstatic.com',
    'style-src': "'self' fonts.googleapis.com 'unsafe-inline'",
    'script-src': "'self' 'unsafe-inline' 'unsafe-eval'",
  };
  return Object.keys(policies).map((key) => `${key} ${policies[key]}`).join(';');
})();

const PERMISSIONS_POLICY = (() => {
  const policies = {
    accelerometer: '',
    'ambient-light-sensor': '',
    autoplay: '',
    battery: '',
    camera: '',
    'display-capture': '',
    'document-domain': '',
    'encrypted-media': '',
    fullscreen: 'self',
    geolocation: 'self',
    gyroscope: '',
    'layout-animations': '',
    'legacy-image-formats': '',
    magnetometer: '',
    microphone: '',
    midi: '',
    notifications: 'self',
    'oversized-images': '',
    payment: '',
    'picture-in-picture': '',
    'publickey-credentials-get': '',
    push: `self "${API_URL}"`,
    speaker: '',
    'sync-xhr': '',
    usb: '',
    vibrate: '',
    'wake-lock': '',
    'xr-spatial-tracking': '',
  };
  return Object.keys(policies).map((key) => `${key}=(${policies[key]})`).join(',');
})();

module.exports = function xFrameOptions(req, res, next) {
  res.setHeader('X-Frame-Options', 'deny');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  res.setHeader('Content-Security-Policy', CONTENT_SECURITY_POLICY);
  res.setHeader('Permissions-Policy', PERMISSIONS_POLICY);
  return next();
};
